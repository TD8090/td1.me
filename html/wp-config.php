<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp1tddb');

/** MySQL database username */
define('DB_USER', 'wp1tddb');

/** MySQL database password */
define('DB_PASSWORD', 'DFJQ&nT=ATwh');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define('FTP_PUBKEY','/home/wp-user/wp_rsa.pub');
define('FTP_PRIKEY','/home/wp-user/wp_rsa');
define('FTP_USER','wp-user');
define('FTP_PASS','');
define('FTP_HOST','127.0.0.1:22');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tN`N9-{#Sk<;+J- S$-!5rwD-RN*mtn{h%Y`=@d~fh3ybj+09[A4Y1oXICdaU2kF');
define('SECURE_AUTH_KEY',  'BtTNN>gR63M&{x+pWh>rgS34%WwPHBSpI/f#mE95tMqLfh?.U|Sh!V4F`I-#P)[7');
define('LOGGED_IN_KEY',    '~/,g4t+6:Ma+qHkei@!-kCFud75:t lTJRQh6)`_{K$#P`J$.aaHYhwg2T^8{)Lk');
define('NONCE_KEY',        'C&)s~<E_$?_Is+;sJFHK;C*J^+Aq>gkBO%b/,2mf<3%2sC9YivPVB-2:-M]u:m$Z');
define('AUTH_SALT',        'N-vzPf#q7Uk%y%7s,b=-=.e+Ek}[<(G0+U@DRZ|XmI+<KJmo6`+&I?-Wq-~C2LDQ');
define('SECURE_AUTH_SALT', ';X`_FL#@Ev=;=|SEw%C/cb(k49)sos?BJ_gM%~1],*aq:0SG|T396Ss)3[G&%0US');
define('LOGGED_IN_SALT',   'Vyr-YPg}<ZP*>6k~Cmi&mhG.65SemUl@+0iQ?J-1H)Ir9HH-6^+_hysLIi l#Rje');
define('NONCE_SALT',       '1WM@gB9;-S99+N&Ly<~p0+=vk .CW~z:fe~7`rQg8}|X]x_6J5Wp_0x?-#$UGW=m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp1table_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
